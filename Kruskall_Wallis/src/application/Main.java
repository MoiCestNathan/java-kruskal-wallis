package application;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;



class RankData {
    double value;
    int rank;

    public RankData(double value, int rank) {
        this.value = value;
        this.rank = rank;
    }
}

public class Main {
    public static void main(String[] args) {
        List<List<Double>> samples = new ArrayList<>();

        List<Double> sample1 = new ArrayList<>();
        sample1.add(2.3);
        sample1.add(1.8);
        sample1.add(2.1);
        // Ajoutez les autres éléments à sample1

        List<Double> sample2 = new ArrayList<>();
        sample2.add(3.9);
        sample2.add(4.2);
        sample2.add(3.5);
        // Ajoutez les autres éléments à sample2

        List<Double> sample3 = new ArrayList<>();
        sample3.add(1.5);
        sample3.add(1.2);
        sample3.add(1.9);
        // Ajoutez les autres éléments à sample3

        samples.add(sample1);
        samples.add(sample2);
        samples.add(sample3);
        
        // Concaténer toutes les données dans une seule liste
        List<Double> allData = new ArrayList<>();
        for (List<Double> sample : samples) {
            allData.addAll(sample);
        }

        // Trier la liste par ordre croissant
        Collections.sort(allData);

        // Attribuer des rangs aux valeurs
        List<RankData> rankedData = new ArrayList<>();
        int rank = 1;
        System.out.println("Valeurs par ordre croissant avec leurs rangs :");
        for (double value : allData) {
            rankedData.add(new RankData(value, rank));
            System.out.println("Valeur : " + value + ", Rang : " + rank);
            rank++;
        }

        // Affecter à chaque échantillon la somme des rangs de ses valeurs
        List<Integer> sampleRankSums = new ArrayList<>();
        for (List<Double> sample : samples) {
            int sampleRankSum = 0;
            for (double value : sample) {
                int valueRank = findRank(rankedData, value);
                sampleRankSum += valueRank;
            }
            sampleRankSums.add(sampleRankSum);
            System.out.println("Somme des rangs pour l'échantillon : " + sampleRankSum);
        }
        
     // Calculer la statistique de test H
        int totalSampleSize = allData.size();
        double N = (double) totalSampleSize;
        double H = 0.0;
        double sumRiSquared = 0.0;
        for (List<Double> sample : samples) {
            double sampleRankSum = 0;
            for (double value : sample) {
                int valueRank = findRank(rankedData, value);
                sampleRankSum += valueRank;
            }
            sumRiSquared += Math.pow(sampleRankSum, 2) / sample.size();
        }
        H = (12.0 / (N * (N + 1))) * sumRiSquared - 3 * (N + 1);
        
        int k = samples.size();
        compareHWithCriticalValue(k, H);
    }

    private static int findRank(List<RankData> rankedData, double value) {
        for (RankData rankData : rankedData) {
            if (rankData.value == value) {
                return rankData.rank;
            }
        }
        return -1; // Valeur non trouvée
    }
    public static void compareHWithCriticalValue(int k, double H) {
        double[] criticalValues = {3.841, 5.991, 7.815, 9.488, 11.070, 12.592, 14.067, 15.507, 16.919};
        
        double criticalValue = criticalValues[k - 2];
        
        System.out.println("Statistique de test H : " + H);
        System.out.println("Valeur critique tα : " + criticalValue);
        
        if (H > criticalValue) {
            System.out.println("H est supérieur à la valeur critique. Rejeter l'hypothèse nulle. On en déduit alors qu’au moins un des trois\r\n"
            		+ "méthode est différente des autres.");
        } else {
            System.out.println("H est inférieur ou égal à la valeur critique. Ne pas rejeter l'hypothèse nulle.");
        }
    }
}
